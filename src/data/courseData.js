const courseData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipi sicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamquis = nostrud exercitation ullamco laboris nisi ut aliquip ex ea comm odoco nsequat. Duis aute irure dolor in reprehende rit in voluptate velit essecillum dolore eu f ugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		price: 55000,
		onOffer: true
	},
		{
		id: "wdc002",
		name: "JAVA Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipis icing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamquis = no strud exercita tion ullamco laboris nisi ut aliquip ex ea commo do consequat. Duis aute irure dolor in reprehenderit in volupt ate velit essecillum dolore eu f ugiat nulla paria tur. Ex  cepte ur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		price: 40000,
		onOffer: true
	},
		{
		id: "wdc003",
		name: "Python Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipisi cing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamquis = nostrud exercitation ullamco laboris nisi ut aliquip ex ea com m d ocon sequat. Duis aute irure dolor in reprehenderit in voluptate velit essecillum dolore eu f ugiat nulla paria tur. Excepteur sint occaecat cupidatat non proident, sunt in  cu lpa qui officia deserunt mollit anim id est laborum.",
		price: 30000,
		onOffer: true
	}
]

export default courseData;