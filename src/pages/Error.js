import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'
export default function Error(){
	return(
		<Row>
			<Col className = "p-5">
				<h1>404 Not Found</h1>
				<p>The page you are looking for cannot be found</p>
				<Button as={Link} to='/' variant="primary">Back Home</Button>
			</Col>
		</Row>
	)
}