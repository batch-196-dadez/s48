import {useState} from 'react'
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function CourseCard(props) {
	// console.log(props);
	// console.log(typeof props);

	// Object destructuring
	const {name, description, price, _id} = props.courseProp;

	// react hooks - useState - store its state
	// Syntax:
		// const[getter, setter] = useState(initialGetterValue)
	// const [count, setCount] = useState(10);
	// console.log(useState(0));
	// const [enrolled, setEnrolled] = useState(0);

	// function enroll (){
	// 	if (count === 0){
	// 		Swal.fire({
	// 				title: "No More Seats Left",
	// 				icon: "error",
	// 				text: "Check Back Later!"
	// 			})
	// 	} else {
	// 		setCount(count - 1);
	// 		setEnrolled(enrolled + 1);
	// 	}
	// }

	return(
		<Card className="cardCourse p-3 mb-3">
			<Card.Body>
				<Card.Title>
					<h2>{name}</h2>
				</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>
				{description}
				</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>
				{price}
				</Card.Text>
				<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
			</Card.Body>
		</Card>
	)
}



